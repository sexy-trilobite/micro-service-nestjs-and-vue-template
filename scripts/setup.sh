#!/bin/sh
#git commit log template

BASEDIR=$(git rev-parse --show-toplevel)/scripts

git config commit.template ${BASEDIR}/.gitmessage.txt
git config --add commit.cleanup strip

# 專案檔名區分大小寫
git config core.ignorecase false

# 全域檔名區分大小寫
git config --global core.ignorecase false

echo "> Set up commit msg template success."
