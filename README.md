<div id="top"></div>

<!-- ABOUT THE PROJECT -->

## About The Project

This is a microservice framework practice project built by vue.js & Nest.js and other tools. We hope that through the implementation of this project, we can have a better understanding of the technical aspects.

For any suggestions or questions about our project, we are very welcome to contact us.

<p align="right">(<a href="#top">back to top</a>)</p>

### Built With

This section should list any major frameworks/libraries used to bootstrap your project.

- [Nest.js](https://nestjs.com/)
- [Vue.js](https://vuejs.org/)
- [Docker](https://www.docker.com/)
- [Graphql](https://graphql.org/)
- [MongoDB](https://www.mongodb.com/cloud/atlas/lp/try2?utm_source=google&utm_campaign=gs_apac_taiwan_search_core_brand_atlas_desktop&utm_term=mongo&utm_medium=cpc_paid_search&utm_ad=e&utm_ad_campaign_id=12212624371&gclid=CjwKCAjwtfqKBhBoEiwAZuesiDkCJ120r70wBktyhTIcXwn9bKObh-DGwTXv0lnjmRMmY5U2cF6TYBoC9zoQAvD_BwE)

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- GETTING STARTED -->

## Getting Started

This is an example of how you may give instructions on setting up your project locally.
To get a local copy up and running follow these simple example steps.

### Prerequisites

This is an example of how to list things you need to use the software and how to install them.

- [Node.js](https://nodejs.org/en/) (>= 14.0.0)
- [Docker Desktop for Mac](https://docs.docker.com/docker-for-mac/install/) (>= 2.3.0)

### Installation

#### 1.Install lerna and Nestjs Cli

```console
$ npm install --global lerna
$ npm install --global @nestjs/cli
```

#### 2.Build Docker Network

```console
$ yarn setup:dev:network
```

#### 3. Install local dependencies

```console
$ yarn bootstrap
```

#### 4. Set up .env file

Copy .env.example ** in the following directory to .env** and fill in the required password.

- infra/mongo/.env.example
  - MONGO_ROOT_PASS
- presentation/gateway/.env.example
- services/service-auth/.env.example
  - DB_ROOT_PASS
  - DB_ADMIN_PASS
- services/service-content-mgm
  - DB_ROOT_PASS
  - DB_ADMIN_PASS

> **Please note: -`MONGO_ROOT_PASS` and `DB_ROOT_PASS` of each Service must be the same**

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- USAGE EXAMPLES -->

## Usage

#### 1. Start Infra

```console
$ yarn start:dev:infra
```

#### 2. Initialize the service

```console
$ yarn setup:dev:services
```

#### 3. Start Service

```console
$ yarn start:dev:services
```

Open the browser and go to the following URL to confirm whether you can see the GraphQL Playground of each service

- [http://localhost:4001/graphql](http://localhost:4001/graphql)
- [http://localhost:4002/graphql](http://localhost:4002/graphql)

#### 4. Start Gateway (Apollo Federation)

```console
$ yarn start:dev:presentation
```

Open the browser and go to the following URL to confirm if you can see the GraphQL Playground of Gateway

- [http://localhost:4000/graphql](http://localhost:4000/graphql)

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- LICENSE -->

## License

Distributed under the MIT License. See `LICENSE.txt` for more information.

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- CONTACT -->

## Contact

Jane - jane0819tw@gmail.com
MaoMao - stefanmao1112@gmail.com

Project Link: [Micro-service NestJs and Vue template](https://gitlab.com/sexy-trilobite/micro-service-nestjs-and-vue-template/-/tree/main)

<p align="right">(<a href="#top">back to top</a>)</p>
