#!/usr/bin/env sh
BASEDIR=$(git rev-parse --show-toplevel)
SERVICEDIR=${BASEDIR}/$1

docker-compose -p trilobite -f ${SERVICEDIR}/docker-compose.yml up -d
