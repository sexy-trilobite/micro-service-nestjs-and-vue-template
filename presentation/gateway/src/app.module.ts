import { Module, UnauthorizedException } from '@nestjs/common';
import { GraphQLGatewayModule, GATEWAY_BUILD_SERVICE } from '@nestjs/graphql';
import { RemoteGraphQLDataSource } from '@apollo/gateway';
import { ConfigModule, ConfigService } from '@nestjs/config';
import fetch from 'node-fetch';
import * as jwt from 'jsonwebtoken';

const getUser = async (token, authServiceUrl) => {
  const USER = ` 
query user ($id: String!) {
  user(id: $id) {
    id
    birth
    name
    account
    status
    globalRoles{
      id
      permissions
    }
    appRoles{
      id
      permissions
    }
  }
}
`;

  const data = await fetch(authServiceUrl, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify({
      query: USER,
      variables: { id: token['userId'] },
    }),
  });

  const {
    data: { user },
  } = await data.json();

  return user;
};


const formatPermissions = (globalRoles, appRoles) => {
  let permissions = [];
  globalRoles.forEach(globalRole => {
    permissions = permissions.concat(globalRole.permissions);
  });
  appRoles.forEach(appRole => {
    permissions = permissions.concat(appRole.permissions);
  });
  return permissions
}

class AuthenticatedDataSource extends RemoteGraphQLDataSource {
  constructor(url: string, private configService: ConfigService) {
    super();
    this.url = url;
    this.configService = configService;
  }
  async willSendRequest({ request, context }) {
    if (context.jwt) {
      let token: string | object;
      try {
        const jwtSecret = this.configService.get<string>('JWT_SECRET');
        token = jwt.verify(context.jwt, jwtSecret);
      } catch (error) {
        console.log(error);
        throw new UnauthorizedException();
      }

      const authServiceUrl = this.configService.get<string>(
        'SERVICE_AUTH_ENDPOINT',
      );

      const user = await getUser(token, authServiceUrl);
      const permissions = formatPermissions(user.globalRoles, user.appRoles)
      request.http.headers.set('user-id', user.id);
      request.http.headers.set('user-permissions', permissions);
    }
  }
}
@Module({
  providers: [
    {
      provide: AuthenticatedDataSource,
      useValue: AuthenticatedDataSource,
    },
    {
      provide: GATEWAY_BUILD_SERVICE,
      useFactory: (AuthenticatedDataSource, configService: ConfigService) => {
        return ({ name, url }) => {
          return new AuthenticatedDataSource(url, configService);
        };
      },
      inject: [AuthenticatedDataSource, ConfigService],
    },
  ],
  exports: [GATEWAY_BUILD_SERVICE],
})
class BuildServiceModule { }

@Module({
  imports: [
    GraphQLGatewayModule.forRootAsync({
      useFactory: async (configService: ConfigService) => {
        const options = {
          server: {
            // ... Apollo server options
            cors: true,
            context: ({ req }) => {
              return ({
                jwt: req.headers.authorization,
              })
            }
          },
          gateway: {
            serviceList: [
              {
                name: 'service-auth',
                url:
                  configService.get<string>('SERVICE_AUTH_ENDPOINT') ||
                  'http://localhost:4001/graphql',
              },
              {
                name: 'service-content-mgm',
                url:
                  configService.get<string>('SERVICE_CONTENT_MGM_ENDPOINT') ||
                  'http://localhost:4002/graphql',
              },
            ],
          },
        };
        return options;
      },
      imports: [BuildServiceModule],
      inject: [ConfigService],
    }),
    ConfigModule.forRoot({ isGlobal: true }),
  ],
})
export class AppModule { }
