import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';
async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const configService = app.get(ConfigService);
  await app.listen(configService.get<string>('APP_PORT'));
  console.log(
    `gateway start at port ${configService.get<string>('APP_PORT')}.`,
  );
}
bootstrap();
