#!/usr/bin/env sh
BASEDIR=$(git rev-parse --show-toplevel)

docker-compose -p trilobite -f ${BASEDIR}/$1/docker-compose.yml stop
