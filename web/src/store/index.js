import Vue from 'vue'
import Vuex from 'vuex'
import modules from './modules'
// Install the vue plugin
Vue.use(Vuex)

export function createStore() {
    /* use vuex modules */
    return new Vuex.Store({
        modules
    })
}