const testPosts = {
    state() {
        return {
            testPosts: null
        }
    },
    mutations: {
        SET_TEST_POSTS(state, criteria) {
            state.testPosts = criteria
        }
    },
    actions: {
        setTestPosts({ commit }, criteria) {
            commit('SET_TEST_POSTS', criteria)
        }
    },
    getters: {
        getTestPosts(state) {
            return state.testPosts
        }
    }
}
export default testPosts;