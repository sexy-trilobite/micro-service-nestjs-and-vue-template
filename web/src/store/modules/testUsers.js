const testUsers = {
    state() {
        return {

            testUsers: null
        }
    },
    mutations: {
        SET_TEST_USERS(state, criteria) {
            state.testUsers = criteria
        }
    },
    actions: {
        setTestUsers({ commit }, criteria) {
            commit('SET_TEST_USERS', criteria)
        }
    },
    getters: {
        getTestUsers(state) {
            return state.testUsers
        }
    }
}
export default testUsers;