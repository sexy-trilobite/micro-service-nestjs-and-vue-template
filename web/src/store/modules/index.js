import testPosts from './testPosts';
import testUsers from './testUsers';
import testValue from './testValue';
import user from './auth/user';

const modules = { testValue, testPosts, testUsers, user };

export default modules;
