import Cookies from 'js-cookie';

const user = {
  state() {
    return {
      user: null,
      accessToken: null,
      refreshToken: null,
      isReVerifyToken: false,
    };
  },
  mutations: {
    SET_USER(state, criteria) {
      state.user = criteria;
    },
    SET_TOKEN(state, criteria) {
      state.accessToken = criteria.accessToken;
      state.refreshToken = criteria.refreshToken;
    },
    CLEAN_CREDENTIAL(state) {
      state.accessToken = null;
      state.refreshToken = null;
      if (typeof window !== 'undefined') {
        Cookies.remove('accessToken');
        Cookies.remove('refreshToken');
      }
    },
    SET_CREDENTIAL(state, { user, accessToken, refreshToken }) {
      state.user = user;
      state.accessToken = accessToken;
      state.refreshToken = refreshToken;

      if (typeof window !== 'undefined') {
        Cookies.set('accessToken', accessToken);
        Cookies.set('refreshToken', refreshToken);
      }
    },
    IS_REVERIFY_TOKEN(state, criteria) {
      state.isReVerifyToken = criteria;
    },
  },
  actions: {
    setUser({ commit }, criteria) {
      commit('SET_USER', criteria);
    },
    setToken({ commit }, criteria) {
      commit('SET_TOKEN', criteria);
    },
    cleanCredential({ commit }) {
      commit('CLEAN_CREDENTIAL');
    },
    isReVerifyToken({ commit }, criteria) {
      commit('IS_REVERIFY_TOKEN', criteria);
    },
    setCredential({ commit }, criteria) {
      commit('SET_CREDENTIAL', criteria);
    },
  },
  getters: {
    getUser(state) {
      return state.user;
    },
    getAccessToken(state) {
      return state.accessToken;
    },
    getRefreshToken(state) {
      return state.refreshToken;
    },
    getIsReVerifyToken(state) {
      return state.isReVerifyToken;
    },
  },
};

export default user;
