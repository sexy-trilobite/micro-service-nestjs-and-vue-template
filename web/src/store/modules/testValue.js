const testValue = {
    state() {
        return {
            testValue: 1
        }
    },
    mutations: {
        ADD_TEST_VALUE(state, criteria) {
            state.testValue += criteria
        }
    },
    actions: {
        addTestValue({ commit }, criteria) {
            commit('ADD_TEST_VALUE', criteria)
        }
    },
    getters: {
        getTestValue(state) {
            return state.testValue
        }
    }
}
export default testValue;