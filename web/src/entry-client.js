import { loadAsyncComponents } from '@akryum/vue-cli-plugin-ssr/client';
import { createApp } from './main';

import Cookies from 'js-cookie';

const apolloStateData = JSON.parse(window.__APOLLO_STATE__).defaultClient;
// console.log('===== entry-client.js =====');
// console.log(JSON.parse(window.__APOLLO_STATE__));
/**
 * The Client entry simply creates the app and mounts it to the DOM
 */
createApp({
  /**
   *  Hook before creating Vue app
   *
   * @param {object} properties
   */
  async beforeApp({ router, apolloProvider, store }) {
    await loadAsyncComponents({ router });

    const isReVerifyToken = apolloStateData.isReVerifyToken;

    if (isReVerifyToken) {
      /**代表accessToken 過期 去query refreshToken */
      const { originalRefreshToken } = apolloStateData;
      const refreshTokenKey = `refreshToken(${JSON.stringify({
        refreshToken: originalRefreshToken,
      })})`;
      const refreshTokenId = apolloStateData.ROOT_MUTATION[refreshTokenKey].id;
      const meUserDtoKey = apolloStateData[refreshTokenId].user.id;

      /**將新的Tokens 存到 vuex */
      /**得到新的me資料 存進vuex */
      const { accessToken, refreshToken } = apolloStateData[refreshTokenId];
      const me = formatMe(apolloStateData, meUserDtoKey);

      store.dispatch('setToken', { accessToken, refreshToken });
      store.dispatch('setUser', me);

      /*寫入新的 Cookie*/
      Cookies.set('accessToken', accessToken);
      Cookies.set('refreshToken', refreshToken);
    } else if (apolloStateData.ROOT_QUERY && !isReVerifyToken) {
      /* 代表accessToken 沒有過期 */
      const token = {
        refreshToken: Cookies.get('refreshToken'),
        accessToken: Cookies.get('accessToken'),
      };
      const meUserDtoKey = apolloStateData.ROOT_QUERY.me.id;
      const me = formatMe(apolloStateData, meUserDtoKey);

      store.dispatch('setToken', token);
      store.dispatch('setUser', me);
    }
  },

  /**
   *  Hook after app created
   *
   * @param {object} properties
   */
  async afterApp({ app, store, apolloProvider }) {
    app.$mount('#app');
  },
});

function formatMe(apolloStateData, meUserDtoKey) {
  let me = apolloStateData[meUserDtoKey];

  me.globalRoles = me.globalRoles.map((globalRole) => {
    return apolloStateData[globalRole.id];
  });

  me.appRoles = me.appRoles.map((appRole) => {
    return apolloStateData[appRole.id];
  });
  return me;
}
