import 'isomorphic-fetch';
import ApolloSSR from 'vue-apollo/ssr';

import { createApp } from './main';
import { ME } from '@/constants/auth/query';

/** The page refresh will be executed here first */
export default (context) => {
  // let storeRefreshToken = null;

  return new Promise(async (resolve, reject) => {
    const { app, router, apolloProvider, store } = await createApp({
      async beforeApp({ store }) {
        if (context.req.headers.cookie) {
          const { accessToken, refreshToken } = getTokensFromCookie(
            context.req.headers.cookie
          );
          /** Store cookies to server's vuex  */
          store.dispatch('setToken', { accessToken, refreshToken });
        }
      },
      async afterApp({ store, apolloProvider }) {
        const storeAccessToken = store.getters.getAccessToken;
        const storeRefreshToken = store.getters.getRefreshToken;
        const me = store.getters.getUser;

        if (storeAccessToken && storeRefreshToken) {
          if (!me) {
            let me = await getMeFromApolloProvider(apolloProvider);
            store.dispatch('setUser', me);
          }
        }
      },
    });

    router.push(context.url).catch((err) => {
      console.log(`ERROR!=>`, err);
    });

    router.onReady(() => {
      context.rendered = () => {
        context.apolloState = formatApolloSSRStates(
          ApolloSSR.getStates(apolloProvider),
          store
        );
      };
      resolve(app);
    }, reject);
  });
};

function getTokensFromCookie(cookie) {
  return {
    accessToken: cookie.split(';')[0].split('=')[1],
    refreshToken: cookie.split(';')[1].split('=')[1],
  };
}

async function getMeFromApolloProvider(apolloProvider) {
  let response = await apolloProvider.defaultClient.query({
    query: ME,
  });
  if (response.data.me) return response.data.me;
}

function formatApolloSSRStates(apolloSSRStates, store) {
  apolloSSRStates.defaultClient.originalRefreshToken = null;
  apolloSSRStates.defaultClient.isReVerifyToken =
    store.getters.getIsReVerifyToken;
  return JSON.stringify(apolloSSRStates);
}
