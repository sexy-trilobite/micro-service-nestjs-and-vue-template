import Vue from 'vue';
import VueApollo from 'vue-apollo';
import {
  createApolloClient,
  restartWebsockets,
} from 'vue-cli-plugin-apollo/graphql-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { errorLink } from '@/utils/error-link';
import { ApolloLink } from '@apollo/client/core';

// Install the vue plugin
Vue.use(VueApollo);

// Name of the localStorage item
const AUTH_TOKEN = 'apollo-token';

// Http endpoint
let HOST =
  process.env.NODE_ENV === 'production'
    ? process.env.VUE_APP_GRAPHQL_HTTP_HOST
    : 'localhost';
let HTTP = process.env.NODE_ENV === 'production' ? 'https' : 'http';
let PORT = process.env.VUE_APP_GRAPHQL_HTTP_PORT;
// 判斷是否為 Server Side Render and Run Container?

if (typeof window === 'undefined') {
  HOST = process.env.VUE_APP_GRAPHQL_DNS
    ? process.env.VUE_APP_GRAPHQL_DNS
    : 'localhost';
  PORT = process.env.VUE_APP_GRAPHQL_PORT;
  HTTP = 'http';
}
const httpEndpoint = `${HTTP}://${HOST}:${PORT}/graphql`;
// config
const defaultOptions = (store, router) => {
  return {
    // URL to the HTTP API
    httpEndpoint,
    // Url to the Websocket API
    wsEndpoint: null,
    // Token used in localstorage
    tokenName: AUTH_TOKEN,
    // Enable this if you use Query persisting with Apollo Engine
    persisting: false,
    // Or, advanced persisting options, see https://github.com/apollographql/apollo-link-persisted-queries#options
    // Example:
    // persisting: {
    //  generateHash: query => sha256()
    //    .update(print(query))
    //    .digest('hex'),
    //},
    // Is currently Server-Side Rendering or not
    ssr: true,
    // Only use Websocket for all requests (including queries and mutations)
    websocketsOnly: false,
    // Custom starting link.
    // If you want to replace the default HttpLink, set `defaultHttpLink` to false
    link: ApolloLink.from([errorLink(store, router)]),
    // Custom pre-auth links
    // Useful if you want, for example, to set a custom middleware for refreshing an access token.
    preAuthLinks: [],
    // If true, add the default HttpLink.
    // Disable it if you want to replace it with a terminating link using `link` option.
    defaultHttpLink: true,
    // Options for the default HttpLink
    httpLinkOptions: {},
    // Custom Apollo cache implementation (default is apollo-cache-inmemory)
    cache: new InMemoryCache(),
    // Options for the default cache
    inMemoryCacheOptions: {},
    // Additional Apollo client options
    apollo: {},
    // apollo-link-state options
    clientState: null,
    // Function returning Authorization header token
    getAuth: (tokenName) => {
      const accessToken = store.getters.getAccessToken;
      // console.log(`====vue-apollo.js getAuth====`);
      // console.log(accessToken);
      return accessToken ? accessToken : null;
    },
  };
};

/**
 * Create apollo client.
 *
 * @param {*} options
 */
export function createProvider(options = {}, store, router) {
  const { apolloClient, wsClient } = createApolloClient({
    ...defaultOptions(store, router),
    ...options,
  });
  apolloClient.wsClient = wsClient;

  // Create a provider
  const apolloProvider = new VueApollo({
    defaultClient: apolloClient,
    errorHandler(error) {
      // eslint-disable-next-line no-console
      console.log(
        `%cError`,
        `background: red; color: white; padding: 2px 4px; border-radius: 3px; font-weight: bold;`,
        error.message
      );
    },
  });
  return apolloProvider;
}

/**
 * Manually call this when user log in.
 *
 * @param {} apolloClient
 * @param {} token
 */
export async function onLogin(apolloClient, token) {
  console.log(`onlogin....`);
  if (typeof localStorage !== 'undefined' && token) {
    // eslint-disable-next-line no-undef
    localStorage.setItem(AUTH_TOKEN, token);
  }
  if (apolloClient.wsClient) restartWebsockets(apolloClient.wsClient);
  try {
    await apolloClient.resetStore();
  } catch (e) {
    // eslint-disable-next-line no-console
    console.log(`%cError on cache reset (login)`, `color: orange;`, e.message);
  }
}

/**
 * Manually call this when user log out.
 *
 * @param {} apolloClient
 */
export async function onLogout(apolloClient) {
  console.log(`onlogout....`);
  if (typeof localStorage !== 'undefined') {
    // eslint-disable-next-line no-undef
    localStorage.removeItem(AUTH_TOKEN);
  }
  if (apolloClient.wsClient) restartWebsockets(apolloClient.wsClient);
  try {
    await apolloClient.resetStore();
  } catch (e) {
    // eslint-disable-next-line no-console
    console.log(`%cError on cache reset (logout)`, `color: orange;`, e.message);
  }
}
