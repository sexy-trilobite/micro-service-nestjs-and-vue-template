import gql from 'graphql-tag';

export const ME = gql`
  query {
    me {
      id
      account
      name
      status
      birth
      globalRoles {
        id
        name
        description
        chi_description
        chi_name
        permissions
      }
      appRoles {
        id
        name
        description
        chi_description
        chi_name
        permissions
      }
    }
  }
`;

export const USERS = gql`
  query users {
    users {
      id
      name
      birth
    }
  }
`;
