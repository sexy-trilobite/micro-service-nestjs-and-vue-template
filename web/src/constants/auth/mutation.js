import gql from 'graphql-tag';

export const SIGNIN = gql`
  mutation signin($input: SigninInput!) {
    signin(input: $input) {
      user {
        id
        account
        birth
        name
        appRoles {
          id
          name
          description
          chi_description
          chi_name
          permissions
        }
        globalRoles {
          id
          name
          description
          chi_description
          chi_name
          permissions
        }
      }
      accessToken
      refreshToken
    }
  }
`;

export const REFRESH_TOKEN = gql`
  mutation refreshToken($refreshToken: String!) {
    refreshToken(refreshToken: $refreshToken) {
      refreshToken
      accessToken
      user {
        id
        account
        birth
        name
        appRoles {
          id
          name
          description
          chi_description
          chi_name
          permissions
        }
        globalRoles {
          id
          name
          description
          chi_description
          chi_name
          permissions
        }
      }
    }
  }
`;
