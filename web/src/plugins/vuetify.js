import 'material-design-icons-iconfont/dist/material-design-icons.css'
import config from 'utils/config';
import Vue from 'vue';
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';


Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    options: {
      customProperties: true,
    },
    themes:{
      light:config.light
    }
  },
  icons: {
    iconfont: 'mdiSvg',
  },
});
