import { onError } from 'apollo-link-error';
import { Observable } from 'apollo-link';
import { REFRESH_TOKEN } from '@/constants/auth/mutation';

export const errorLink = (store, router) => {
  return onError(({ graphQLErrors, operation, forward }) => {
    if (graphQLErrors && graphQLErrors[0].message === 'Unauthorized') {
      return promiseToObservable(
        getRefreshToken(store, operation),
        store,
        router
      ).flatMap(() => forward(operation));
    }
  });
};

const getRefreshToken = (store, operation) => {
  const refreshToken = store.getters.getRefreshToken;
  store.dispatch('cleanCredential');
  return store.provider.defaultClient
    .mutate({
      mutation: REFRESH_TOKEN,
      variables: {
        refreshToken,
      },
    })
    .then(({ data }) => {
      //   store.dispatch('isReVerifyToken', true);
      const { refreshToken, accessToken, user } = data.refreshToken;
      store.dispatch('setCredential', {
        user,
        refreshToken,
        accessToken,
      });
      const oldHeaders = operation.getContext().headers;
      operation.setContext({
        headers: {
          ...oldHeaders,
          authorization: accessToken,
        },
      });
    });
};

const promiseToObservable = (promise, store, router) =>
  new Observable((subscriber) => {
    promise.then(
      (value) => {
        if (subscriber.closed) return;
        subscriber.next(value);
        subscriber.complete();
      },
      (err) => {
        router.push({
          path: '/',
          query: { redirect: router.currentRoute.path },
        });
        store.dispatch('isReVerifyToken', false);
        subscriber.complete();
      }
    );
    return subscriber; // this line can removed, as per next comment
  });
