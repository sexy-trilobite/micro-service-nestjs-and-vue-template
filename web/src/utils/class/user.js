export default class User {
  constructor(store) {
    this.store = store;
    this.data = null;
    this.isLogin = this.isLogIn();
    if (this.isLogin) {
      this.data = this._formatUserData();
    }
  }

  isLogIn() {
    const { getAccessToken, getRefreshToken } = this.store.getters;
    return getAccessToken && getRefreshToken ? true : false;
  }

  //check user Permissions
  isUserIncludeRoles(permissionRoles) {
    const intersection = this.data.roles.filter((role) =>
      permissionRoles.includes(role.name)
    );
    return intersection.length ? true : false;
  }
  // Merge approles and global roles

  _formatUserData() {
    const { name, account, appRoles, globalRoles } = this.store.getters.getUser;
    const roles = [];
    appRoles.forEach((role) => {
      roles.push(role);
    });
    globalRoles.forEach((role) => {
      roles.push(role);
    });

    let user = {
      account,
      name,
      account,
      roles,
    };
    return user;
  }
}
