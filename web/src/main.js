import Vue from 'vue';
import App from './App.vue';
import vuetify from '@/plugins/vuetify';
import { createProvider } from './vue-apollo';
import { createRouter } from './router';
import { createStore } from './store';

import Guard from '@/router/router-guard/navigation-guard';
// import User from '@/utils/class/user';

Vue.config.productionTip = false;

if (typeof window !== 'undefined') {
  const Vuelidate = require('vuelidate');
  Vue.use(Vuelidate);
}

/**
 * Function to create Vue app
 * export a factory function for creating fresh app ,router and store Instances
 * @param {object} hooks
 */

export async function createApp({
  beforeApp = () => {},
  afterApp = () => {},
} = {}) {
  const store = createStore();
  const router = createRouter();
  const resolvers = {};
  const apolloProvider = createProvider(
    {
      ssr: process.server,
      resolvers,
    },
    store,
    router
  );

  store.provider = apolloProvider;

  await beforeApp({
    router,
    store,
    apolloProvider,
  });

  /**Router Guard */
  Guard(router, store);

  const app = new Vue({
    router,
    store,
    apolloProvider,
    vuetify,
    render: (h) => h(App),
  });

  const result = {
    app,
    router,
    apolloProvider,
    store,
  };

  await afterApp(result);

  return result;
}
