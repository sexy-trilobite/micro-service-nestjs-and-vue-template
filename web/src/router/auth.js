import TestAuth from 'views/TestAuth';

const authRoutes = [
  {
    path: '/test-auth',
    name: 'TestAuth',
    component: TestAuth,
    meta: {
      text: 'test-auth',
      requiresAuth: true,
      roles: ['auth_admin'],
    },
  },
];
export default authRoutes;
