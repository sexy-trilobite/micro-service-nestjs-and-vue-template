import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from 'views/Home';
import Login from 'views/Login';

import authRoutes from './auth';
import contentMgmRoutes from './content-mgm';
import errorPageRoutes from './errorPage';

/** FIX NavigationDuplicated Navigating Problem  */
const originalPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch((err) => err);
};

// Install the vue plugin
Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    props: { msg: 'Home' },
    meta: {
      text: 'Home',
      requiresAuth: false,
      role: [],
    },
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
    props: { msg: 'Login' },
    meta: {
      text: 'Login',
      requiresAuth: false,
      role: [],
    },
  },
  ...authRoutes,
  ...contentMgmRoutes,
  ...errorPageRoutes,
];

const Router = new VueRouter({
  mode: 'history',
  routes,
});

export function createRouter() {
  return Router;
}
