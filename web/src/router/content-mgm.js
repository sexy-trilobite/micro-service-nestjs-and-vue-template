import TestContentMgm from 'views/TestContentMgm';

const contentMgmRoutes = [
  {
    path: '/test-content-mgm',
    name: 'TestContentMgm',
    component: TestContentMgm,
    meta: {
      text: 'test-content-mgm',
      requiresAuth: true,
      roles: ['auth_general_user'],
    },
  },
];
export default contentMgmRoutes;
