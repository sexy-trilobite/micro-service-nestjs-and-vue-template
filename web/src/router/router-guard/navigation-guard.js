import User from '@/utils/class/user';
import Cookies from 'js-cookie';
import user from '../../store/modules/auth/user';

export default function Guard(Router, Store) {
  Router.beforeEach(async (to, from, next) => {
    const user = new User(Store);

    /** 目前沒有 Login 頁面先用Home 代替 */
    if (to.name !== 'Login') {
      if (to.matched.length === 0) {
        /** If route is not exist */
        next({ name: 'PageNotFound' });
      } else {
        if (to.meta.requiresAuth) {
          /** If Page needs Permissions */
          if (!user.isLogin) {
            next({ name: 'Login', query: { redirect: to.fullPath } });
          } else {
            if (to.name === 'Home' && user.isUserIncludeRoles(['auth_admin'])) {
              next({ name: 'TestAuth' });
            } else {
              if (!user.isUserIncludeRoles(to.meta.roles)) {
                next({ name: 'PermissionFribben' });
              }
            }
          }
        }
      }
    } else {
      if (user.isLogin) {
        if (user.isUserIncludeRoles(['auth_admin'])) {
          next({ name: 'TestAuth' });
          return;
        } else if (user.isUserIncludeRoles(['auth_general_user'])) {
          next({ name: 'Home' });
        }
      }
    }

    next();
  });
}
