import PageNotFound from 'views/error/404';
import PermissionFribben from 'views/error/403';

const errorPageRoutes = [
  {
    path: '/permission-fribben',
    name: 'PermissionFribben',
    component: PermissionFribben,
    meta: {
      text: 'PermissionFribben',
      requiresAuth: false,
      roles: [],
    },
  },
  {
    path: '/page-not-found',
    name: 'PageNotFound',
    component: PageNotFound,
    meta: {
      text: 'PageNotFound',
      requiresAuth: false,
      roles: [],
    },
  },
];
export default errorPageRoutes;
