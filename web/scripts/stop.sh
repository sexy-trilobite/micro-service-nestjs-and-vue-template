#!/usr/bin/env sh
BASEDIR=$(git rev-parse --show-toplevel)

docker-compose -p trilobite -f ${BASEDIR}/web/docker-compose.yml stop
