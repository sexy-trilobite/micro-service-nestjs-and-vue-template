const path = require('path');

function resolve(dir) {
  return path.join(__dirname, dir);
}

module.exports = {
  configureWebpack: {
    performance: {
      hints: 'warning',
      maxAssetSize: 102400000, // (byte)  => 102400000 bytes = 10000KiB =10MiB
      maxEntrypointSize: 102400000,
      assetFilter: function (assetFilename) {
        // 提供资源文件名的断言函数
        // 只给出js与css文件的性能提示
        return assetFilename.endsWith('.css') || assetFilename.endsWith('.js');
      },
    },
  },
  chainWebpack: (config) => {
    config.resolve.alias
      .set('@', resolve('src'))
      .set('assets', resolve('src/assets'))
      .set('components', resolve('src/components'))
      .set('constants', resolve('src/constants'))
      .set('layout', resolve('src/layout'))
      .set('scss', resolve('src/scss'))
      .set('store', resolve('src/store'))
      .set('utils', resolve('src/utils'))
      .set('views', resolve('src/views'));
  },
  // devServer: {
  //   port: process.env.APP_PORT,
  //   proxy: 'http://localhost:4002/',
  // },
  css: {
    loaderOptions: {
      sass: {
        prependData: `
          @import "@/scss/_index.scss";
        `,
      },
    },
  },
  pluginOptions: {
    ssr: {
      // Listening port for `serve` command
      port: process.env.APP_PORT,
      // Listening host for `serve` command
      host: process.env.APP_HOST,
      // Default title
      defaultTitle: 'Sexy Trilobite Micro Service Web',
    },
    apollo: {
      lintGQL: true,
    },
  },
};
