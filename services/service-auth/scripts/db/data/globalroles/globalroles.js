const ObjectID = require('mongodb').ObjectID;

let globalRoles = [
    {
        id: new ObjectID('f4e6cb338245de2e203a1a3d'),
        name: 'auth_admin',
        description: "manage all users' accounts",
        chi_name: "權限管理員",
        chi_description: "可以管理所有用戶帳號",
        permissions: [new ObjectID('5ac62e238245de2e20435431')],
    },
    {
        id: new ObjectID('f4e6cb338245de2e203a1b4d'),
        name: 'auth_general_user',
        description: "manage personal account",
        chi_name: "權限一般用戶",
        chi_description: "可以管理自己的帳號資訊",
        permissions: [new ObjectID('5ac62e238245de2e20435432')],
    }
];

module.exports = globalRoles;
