const ObjectID = require('mongodb').ObjectID;
/* permissions ID is used in PermissionEnum */
let permissions = [
    {
        id: new ObjectID('5ac62e238245de2e20435431'),
        name: 'admin_account_manage',
        description: "Manage all users' account",
    },
    {
        id: new ObjectID('5ac62e238245de2e20435432'),
        name: 'personal_account_manage',
        description: 'Manage personal account.',
    },

    {
        id: new ObjectID('5ac62e238245de2e20435433'),
        name: 'admin_content_manage',
        description: 'Manage all contents in CONTENT_MGM.',
    },
    {
        id: new ObjectID('5ac62e238245de2e20435434'),
        name: 'personal_content_manage',
        description: 'GManage personal contents in CONTENT_MGM.',
    },
];

module.exports = permissions;