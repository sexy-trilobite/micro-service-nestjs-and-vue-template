const ObjectID = require('mongodb').ObjectID;

module.exports = [
  {
    id: new ObjectID('60e192e13153a63c5b4ed752'),
    name: 'sexy-trilobite', // admin
    birth: new Date(),
    account: 'trilobite001',
    password: '$2a$10$rf3xXWQIrR.k2S0.yrMH1eI3FdkL4Chl7nWsmvYPgvNamYsk5LAva', // test123 hash
    status: 'ACTIVE',
    globalRoles: [new ObjectID('f4e6cb338245de2e203a1a3d')], // auth_admin
    appRoles: [new ObjectID('5ffc052f3f8cfc897e75e617')], // content_mgm_admin
  },
  {
    id: new ObjectID('60e192e95966a3239a6f9045'),
    name: 'sexy-trilobite2',
    birth: new Date(),
    account: 'trilobite002',
    password: '$2a$10$rf3xXWQIrR.k2S0.yrMH1eI3FdkL4Chl7nWsmvYPgvNamYsk5LAva',
    status: 'ACTIVE',
    globalRoles: [new ObjectID('f4e6cb338245de2e203a1b4d')], // auth_general_user
    appRoles: [new ObjectID('34f5a1238245de2e203a7422')], // content_mgm_user
  },
];
