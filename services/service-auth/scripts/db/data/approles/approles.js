
const ObjectID = require('mongodb').ObjectID;

let appRoles = [
    {
        id: new ObjectID('5ffc052f3f8cfc897e75e617'),
        name: "content_mgm_admin",
        description: "manage all users' contents",
        chi_name: "內容管理員",
        chi_description: "可以管理所有用戶內容",
        application: '5ac62e238245de2e203a7421',
        permissions: [new ObjectID('5ac62e238245de2e20435433')],
    },
    {
        id: new ObjectID('34f5a1238245de2e203a7422'),
        name: "content_mgm_user",
        description: "manage  personal contents",
        chi_name: "內容一般用戶",
        chi_description: "可以管理個人發布的內容",
        application: '5ac62e238245de2e203a7421',
        permissions: [new ObjectID('5ac62e238245de2e20435434')],
    },
];

module.exports = appRoles;
