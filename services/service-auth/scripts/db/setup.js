const path = require('path');
const dotenv = require('dotenv');
const MongoClient = require('mongodb').MongoClient;
const { Seeder } = require('mongo-seeding');

dotenv.config({ path: path.resolve(__dirname, '../../.env') });
const {
    DB_HOST,
    DB_PORT,
    DB_NAME,
    DB_ROOT_USER,
    DB_ROOT_PASS,
    DB_ADMIN_USER,
    DB_ADMIN_PASS,

  } = process.env;


async function createMongoClient(url){
  /* Create a new MongoClient*/
  let client = new MongoClient(url, { useUnifiedTopology: true });
  /* connect client */
  return  await client.connect()

}

async function addAdminUser(client) {
   /* get admin db and get its admin  */
  let adminDbAdmin = await client.db('admin').admin();
  const { databases } = await adminDbAdmin.listDatabases();
  console.log(databases)
  if (!databases.find((db) => db.name === 'service_auth')) {
      /* Create admin user for the service database*/

      db = await client.db('service_auth');
      console.log(db)
      await db.addUser(DB_ADMIN_USER, DB_ADMIN_PASS, {
        roles: [
          {
            role: 'readWrite',
            db: 'service_auth',
          },
          {
            role: 'dbAdmin',
            db: 'service_auth',
          },
        ],
      });
    
    }
}

async function seedingCollections(url) {
  const seeder = new Seeder({
    database: url,
    dropDatabase: true,
  });

  const collectionReadingOptions = {
      transformers: [
        Seeder.Transformers.replaceDocumentIdWithUnderscoreId,
      ]
    };
    
  const collections = seeder.readCollectionsFromPath(
      path.resolve('./scripts/db/data'),
      collectionReadingOptions
  );
  console.log(collections)
  await seeder.import(collections)
}

async function main() {
  const url = `mongodb://${DB_ROOT_USER}:${DB_ROOT_PASS}@${DB_HOST}:${DB_PORT}/${DB_NAME}?authSource=admin`;
  let client = await createMongoClient(url);
  try {
    await addAdminUser(client);
    await seedingCollections(url);
  } catch (err) {
     console.log(err);
  } finally {
    client.close();
  }
}

main();