import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Token, TokenSchema } from './model';
import { TokenService } from './service/token.service';

import { GlobalRoleModule } from '../global-role/global-role.module';
import { AppRoleModule } from '../app-role/app-role.module';
@Module({
  imports: [
    MongooseModule.forFeature([{ name: Token.name, schema: TokenSchema }]),
    GlobalRoleModule,
    AppRoleModule,
  ],
  providers: [TokenService],
  exports: [TokenService],
})
export class TokenModule {}
