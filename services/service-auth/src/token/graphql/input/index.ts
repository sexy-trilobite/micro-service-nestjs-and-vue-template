import { CreateTokenInput } from './create-token.input';
import { UpdateTokenInput } from './update-token.input';

export { CreateTokenInput, UpdateTokenInput };
