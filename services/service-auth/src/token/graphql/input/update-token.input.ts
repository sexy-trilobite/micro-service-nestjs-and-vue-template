import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class UpdateTokenInput {
  @Field()
  refreshToken?: string;

  @Field()
  resetToken?: string;
}
