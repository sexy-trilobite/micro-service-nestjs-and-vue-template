import { Field, InputType, ID } from '@nestjs/graphql';

@InputType()
export class CreateTokenInput {
  @Field()
  userId: string;

  @Field({ nullable: true })
  refreshToken?: string;

  @Field({ nullable: true })
  resetToken?: string;
}
