import { Field, ID, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class TokenDto {
  @Field(() => ID)
  id: string;

  @Field(() => ID)
  userId?: string;

  @Field()
  refreshToken?: string;

  @Field()
  resetToken?: string;
}
