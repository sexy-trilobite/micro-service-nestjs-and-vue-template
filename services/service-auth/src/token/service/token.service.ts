import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';

import { Token } from '../model';
@Injectable()
export class TokenService {
  constructor(
    @InjectModel(Token.name)
    private readonly tokenModel: Model<Token>,
  ) {}
  createToken(input): Promise<Token> {
    return new this.tokenModel(input).save();
  }

  updateTokenById(id: string, token) {
    return this.tokenModel.findByIdAndUpdate(id, token, { new: true });
  }

  findTokenById(id: string): Promise<Token> {
    return this.tokenModel.findById(id).exec();
  }

  findTokenByUserId(userId: string): Promise<Token> {
    return this.tokenModel.findOne({ userId }).exec();
  }

  updateTokenByUserId(userId: string, token) {
    return this.tokenModel.updateOne({ userId }, { $set: token });
  }

  async generateResetToken({ userId, resetToken }) {
    const token = await this.findTokenByUserId(userId);
    if (token) {
      await this.updateTokenById(token.id, { resetToken });
    } else {
      const newToken = { userId, resetToken };

      await this.createToken(newToken);
    }
  }

  async generateRefreshToken({ userId, refreshToken }) {
    const token = await this.findTokenByUserId(userId);
    if (token) {
      await this.updateTokenById(token.id, { refreshToken });
    } else {
      const newToken = { userId, refreshToken };

      await this.createToken(newToken);
    }
  }
}
