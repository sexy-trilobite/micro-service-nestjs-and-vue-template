import { Document, Types } from "mongoose";
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema()
export class GlobalRole extends Document {
    @Prop()
    name: string;

    @Prop()
    description: string;
    @Prop()
    chi_name: string;
    @Prop()
    chi_description: string;

    /* this type of value is Types.ObjectId */
    @Prop({ type: [{ type: Types.ObjectId, ref: 'Permission' }] })
    permissions: string[];


}

export const GlobalRoleSchema = SchemaFactory.createForClass(GlobalRole);
