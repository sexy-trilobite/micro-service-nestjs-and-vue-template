import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { GlobalRole } from '../model';
import { Permission } from '../../permission/model';
@Injectable()
export class GlobalRoleService {
    constructor(
        @InjectModel(GlobalRole.name) private globalRoleModel: Model<GlobalRole>,
        @InjectModel(Permission.name) private permissionModel: Model<Permission>) { }

    async findAll() {
        return await this.globalRoleModel.find().populate('permissions', this.permissionModel).exec();
    }
}
