import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { GlobalRoleService } from './service/global-role.service';
import { GlobalRoleResolver } from './resolver/global-role.resolver';
import { GlobalRole, GlobalRoleSchema } from './model'
import { Permission, PermissionSchema } from '../permission/model'
@Module({
  imports: [
    MongooseModule.forFeature([{ name: GlobalRole.name, schema: GlobalRoleSchema }]),
    MongooseModule.forFeature([{ name: Permission.name, schema: PermissionSchema }]),
  ],
  providers: [GlobalRoleService, GlobalRoleResolver]
})
export class GlobalRoleModule { }
