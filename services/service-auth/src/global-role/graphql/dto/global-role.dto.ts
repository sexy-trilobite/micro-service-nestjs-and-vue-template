import { Directive, Field, ObjectType, ID } from '@nestjs/graphql';
import { PermissionDto } from '../../../permission/graphql/dto';

@Directive(`@key(fields: "id")`)
@ObjectType()
export class GlobalRoleDto {
  @Field(() => ID)
  id: string;

  @Field()
  name: string;

  @Field()
  description: string;

  @Field()
  chi_name: string;

  @Field()
  chi_description: string;

  // @Field(() => [PermissionDto])
  // permissions: [PermissionDto];
  @Field(() => [String])
  permissions: [string];
}
