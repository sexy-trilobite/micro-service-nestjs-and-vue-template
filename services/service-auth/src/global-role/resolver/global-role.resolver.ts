import { Query, Resolver } from '@nestjs/graphql';
import { GlobalRoleService } from '../service/global-role.service';

import { GlobalRoleDto } from '../graphql/dto'
@Resolver(() => GlobalRoleDto)
export class GlobalRoleResolver {
    constructor(private readonly globalRoleService: GlobalRoleService) { }

    @Query(() => [GlobalRoleDto])
    async globalRoles() {
        return await this.globalRoleService.findAll();
    }

}
