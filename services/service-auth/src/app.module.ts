import { join } from 'path';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { GraphQLFederationModule } from '@nestjs/graphql';
import { ConfigModule, ConfigService } from '@nestjs/config';

import { UserModule } from './user/user.module';
import { GlobalRoleModule } from './global-role/global-role.module';
import { PermissionModule } from './permission/permission.module';
import { ApplicationModule } from './application/application.module';
import { AppRoleModule } from './app-role/app-role.module';
import { TokenModule } from './token/token.module';

@Module({
  imports: [
    UserModule,
    GraphQLFederationModule.forRoot({
      autoSchemaFile: join(process.cwd(), 'src/schema.gql'),
    }),

    ConfigModule.forRoot({
      isGlobal: true,
    }),
    MongooseModule.forRootAsync({
      useFactory: async (configService: ConfigService) => {
        const auth = `${configService.get<string>(
          'DB_ADMIN_USER',
        )}:${configService.get<string>('DB_ADMIN_PASS')}`;
        const host = `${configService.get<string>(
          'DB_HOST',
        )}:${configService.get<string>('DB_PORT')}`;
        const db = configService.get<string>('DB_NAME');
        const uri = `mongodb://${auth}@${host}/${db}`;
        return {
          uri,
          useNewUrlParser: true,
          useUnifiedTopology: true,
          useFindAndModify: false,
        };
      },
      inject: [ConfigService],
    }),
    GlobalRoleModule,
    PermissionModule,
    ApplicationModule,
    AppRoleModule,
    TokenModule,
  ],
  controllers: [],
})
export class AppModule {}
