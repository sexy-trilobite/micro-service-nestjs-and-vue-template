import { applyDecorators, UseGuards, SetMetadata } from '@nestjs/common';

import { AuthGuard, PermissionsGuard } from '../guard';
export function Auth(...permissions: string[]) {
    return applyDecorators(
        SetMetadata('permissions', permissions),
        UseGuards(AuthGuard, PermissionsGuard),

    );
}

/* https://docs.nestjs.com/custom-decorators#decorator-composition */