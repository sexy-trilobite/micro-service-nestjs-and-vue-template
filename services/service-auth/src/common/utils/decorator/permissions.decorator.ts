import { SetMetadata } from '@nestjs/common';

export const Permissions = (...permissions: string[]) => SetMetadata('permissions', permissions);

/* https://docs.nestjs.com/fundamentals/execution-context#reflection-and-metadata */

/* Use like:  @Permissions('adminnn') in graphql api*/

/* apply into auth.decorator.ts */