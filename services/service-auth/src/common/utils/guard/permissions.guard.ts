
import { CanActivate, ExecutionContext, Injectable, UnauthorizedException } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { Reflector } from '@nestjs/core'

@Injectable()
export class PermissionsGuard implements CanActivate {
    constructor(private reflector: Reflector) { }

    canActivate(context: ExecutionContext): boolean {
        console.log(`=> permissions.guard.ts`)
        const ctx = GqlExecutionContext.create(context);
        console.log(`checking permission...`)
        // console.log(ctx.getContext().req.headers)

        /*get setMetadata key `permissions` value*/
        const apiPermissions = this.reflector.get<string[]>('permissions', context.getHandler());
        const userPermissions = ctx.getContext().req.user.permissions;
        console.log(`apiPermissions===>`, apiPermissions)
        console.log(`userPermissions===>`, userPermissions)

        if (!apiPermissions.length) return true;
        return this.checkPermissions(apiPermissions, userPermissions)
    }

    checkPermissions(apiPermissions: string[], userPermissions: string[]) {
        for (const apiPermission of apiPermissions) {
            if (userPermissions.indexOf(apiPermission) !== -1) return true
        }
        let ERR_MESSAGE = 'Your authorization is not enough.'
        console.log(ERR_MESSAGE)
        throw new UnauthorizedException(ERR_MESSAGE);
    }
}


/* https://docs.nestjs.com/graphql/other-features#execution-context */
