import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';

@Injectable()
export class AuthGuard implements CanActivate {
  getRequest(context: ExecutionContext) {
    const ctx = GqlExecutionContext.create(context);
    return ctx.getContext().req;
  }

  canActivate(context: ExecutionContext): boolean {
    console.log(`=> auth.guard.ts`);
    const request = this.getRequest(context);

    /* gateway should set params in headers */
    /*check headers params*/
    // console.log(`==request.headers==`)
    console.log(request.headers);
    if (!request.headers['user-id']) {
      throw new UnauthorizedException('Authorization header not found.');
    }

    const user = {
      id: request.headers['user-id'],
      permissions: request.headers['user-permissions'].split(',') || [],
    };
    request.user = user;
    return true;
  }
}

/* https://docs.nestjs.com/graphql/other-features#execution-context */
