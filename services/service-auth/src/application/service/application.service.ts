import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Inject, Injectable } from '@nestjs/common';
import { Application } from '../model';
@Injectable()
export class ApplicationService {
    constructor(@InjectModel(Application.name) private applicationModel: Model<Application>) { }
    async findAll() {
        return await this.applicationModel.find().exec();
    }
}
