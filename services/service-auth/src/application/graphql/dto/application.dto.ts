import { Field, ObjectType, ID } from '@nestjs/graphql';
@ObjectType()
export class ApplicationDto {
    @Field(() => ID)
    id: string;

    @Field()
    name: string;


}
