import { Resolver, Query } from '@nestjs/graphql';
import { ApplicationService } from '../service/application.service';
import { ApplicationDto } from '../graphql/dto';
@Resolver(() => ApplicationDto)
export class ApplicationResolver {
    constructor(private readonly applicationService: ApplicationService) { }

    @Query(() => [ApplicationDto])
    async applications() {
        return await this.applicationService.findAll();
    }
}
