import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ApplicationResolver } from './resolver/application.resolver';
import { ApplicationService } from './service/application.service';
import { Application, ApplicationSchema } from './model';
@Module({
  imports: [
    MongooseModule.forFeature([{ name: Application.name, schema: ApplicationSchema }]),
  ],
  providers: [ApplicationResolver, ApplicationService]
})
export class ApplicationModule { }
