import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, } from 'mongoose';

@Schema()
export class Application extends Document {
    @Prop()
    name: string;


}

export const ApplicationSchema = SchemaFactory.createForClass(Application);