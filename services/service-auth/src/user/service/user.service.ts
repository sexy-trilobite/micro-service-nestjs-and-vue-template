import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { User } from '../model';
import { GlobalRole } from '../../global-role/model';
import { AppRole } from '../../app-role/model';
import { Application } from '../../application/model';
@Injectable()
export class UserService {
  constructor(
    @InjectModel(User.name) private userModel: Model<User>,
    @InjectModel(GlobalRole.name) private globalRoleModel: Model<GlobalRole>,
    @InjectModel(AppRole.name) private appRoleModel: Model<AppRole>,
    @InjectModel(Application.name) private applicationModel: Model<Application>,
  ) {}

  async findUserById(id: string) {
    return await this.userModel
      .findById(id)
      .populate([
        { path: 'globalRoles', model: this.globalRoleModel },
        {
          path: 'appRoles',
          model: this.appRoleModel,
          populate: [
            {
              path: 'application',
              model: this.applicationModel,
            },
          ],
        },
      ])
      .exec();
  }

  async findAll() {
    return await this.userModel
      .find()
      .populate([
        { path: 'globalRoles', model: this.globalRoleModel },
        {
          path: 'appRoles',
          model: this.appRoleModel,
          populate: [
            {
              path: 'application',
              model: this.applicationModel,
            },
          ],
        },
      ])

      .exec();
  }

  async findUserByAccount(account: string) {
    return await this.userModel
      .findOne({ account })
      .populate([
        { path: 'globalRoles', model: this.globalRoleModel },
        {
          path: 'appRoles',
          model: this.appRoleModel,
          populate: [
            {
              path: 'application',
              model: this.applicationModel,
            },
          ],
        },
      ])
      .exec();
  }
}
