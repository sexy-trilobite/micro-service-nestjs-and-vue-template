import { Field, InputType } from "@nestjs/graphql";

@InputType()
export class SigninInput{
    @Field(()=>String)
    account: string

    @Field(()=> String)
    password: String
}