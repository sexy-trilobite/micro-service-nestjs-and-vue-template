import { Field, ObjectType, ID } from '@nestjs/graphql';
import { UserStatus } from '../constants';
import { GlobalRoleDto } from '../../../global-role/graphql/dto';
import { AppRoleDto } from '../../../app-role/graphql/dto';
@ObjectType()
export class UserDto {
  @Field(() => ID)
  id: string;

  @Field()
  name: string;

  @Field(() => Date)
  birth: Date;

  @Field()
  account: string;

  @Field((type) => UserStatus)
  status: UserStatus;

  @Field(() => [AppRoleDto])
  appRoles: [AppRoleDto];

  @Field(() => [GlobalRoleDto])
  globalRoles: [GlobalRoleDto];
}
