import { UserDto } from './user.dto';
import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class SigninDto {
  @Field()
  accessToken: string;

  @Field()
  refreshToken: string;

  @Field((type) => UserDto)
  user: UserDto;
}
