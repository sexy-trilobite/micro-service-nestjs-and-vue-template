import { Document, Types } from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { UserStatus } from '../graphql/constants';

@Schema()
export class User extends Document {
  @Prop()
  name: string;

  @Prop()
  birth: Date;

  @Prop({ lowercase: true })
  account: string

  @Prop()
  password: string

  @Prop()
  status: UserStatus
  @Prop({ type: [{ type: Types.ObjectId, ref: 'GlobalRole' }] })
  globalRoles: string[];

  @Prop({ type: [{ type: Types.ObjectId, ref: 'AppRole' }] })
  appRoles: string[];

}

export const UserSchema = SchemaFactory.createForClass(User);


/* https://docs.nestjs.com/techniques/mongodb */