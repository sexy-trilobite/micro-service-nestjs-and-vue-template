import { Module, forwardRef } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { UserService } from './service/user.service';
import { UserResolver } from './resolver/user.resolver';

import { User, UserSchema } from './model/user.model';

import { GlobalRole, GlobalRoleSchema } from '../global-role/model';
import { AppRole, AppRoleSchema } from '../app-role/model';
import { Application, ApplicationSchema } from '../application/model';

import { jwtModule } from 'src/jwt/jwt.module';
import { TokenModule } from 'src/token/token.module';
import { AppRoleModule } from 'src/app-role/app-role.module';
import { GlobalRoleModule } from 'src/global-role/global-role.module';
@Module({
  imports: [
    MongooseModule.forFeature([
      { name: User.name, schema: UserSchema },
      { name: AppRole.name, schema: AppRoleSchema },
      { name: GlobalRole.name, schema: GlobalRoleSchema },
      { name: Application.name, schema: ApplicationSchema },
    ]),
    forwardRef(() => jwtModule),
    forwardRef(() => AppRoleModule),
    forwardRef(() => GlobalRoleModule),
    TokenModule,
  ],
  providers: [UserService, UserResolver],
})
export class UserModule {}
