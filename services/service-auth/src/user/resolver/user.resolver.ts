import * as bcrypt from 'bcrypt-nodejs';

import { PermissionsGuard } from '../../common/utils/guard';
import { UseGuards } from '@nestjs/common';

import { Query, Resolver, Mutation, Args } from '@nestjs/graphql';
import {
  BadRequestException,
  ForbiddenException,
  NotFoundException,
} from '@nestjs/common';

import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { UserService } from 'src/user/service/user.service';
import { TokenService } from 'src/token/service/token.service';

import { User } from '../model';
import { UserDto, SigninDto } from '../graphql/dto';
import { SigninInput } from '../graphql/input';
import { UserStatus } from '../graphql/constants';

import { CurrentUser, Auth, Permissions } from '../../common/utils/decorator';

import { PermissionEnum } from '../../permission/graphql/constants';

@Resolver(() => UserDto)
export class UserResolver {
  constructor(
    private readonly userService: UserService,
    private readonly configService: ConfigService,
    private readonly jwtService: JwtService,
    private readonly tokenService: TokenService,
  ) {}

  signAccessToken(user: User) {
    const accessTokenExpireIn = this.configService.get<string>(
      'JWT_ACCESS_TOKEN_EXPIREIN',
    );
    return this.jwtService.sign(
      { userId: user.id },
      { expiresIn: accessTokenExpireIn },
    );
  }

  signRefreshToken(user: User) {
    const refreshTokenExpireIn = this.configService.get<string>(
      'JWT_REFRESH_TOKEN_EXPIREIN',
    );
    return this.jwtService.sign(
      { userId: user.id },
      { expiresIn: refreshTokenExpireIn },
    );
  }

  // @Auth(PermissionEnum.PERSONAL_CONTENT_MANAGE, PermissionEnum.ADMIN_ACCOUNT_MANAGE)
  @Auth(PermissionEnum.ADMIN_ACCOUNT_MANAGE)
  // @UseGuards(PermissionsGuard)
  // @Permissions('adminnn')
  @Query(() => [UserDto])
  async users() {
    return await this.userService.findAll();
  }

  @Query(() => UserDto)
  async user(@Args('id') id: string) {
    const user = await this.userService.findUserById(id);

    if (!user) {
      throw new NotFoundException(id);
    }
    return user;
  }

  @Query(() => UserDto)
  async me(@CurrentUser() currentUser: User) {
    const user = await this.userService.findUserById(currentUser.id);
    return user;
  }

  @Mutation(() => SigninDto)
  async signin(@Args('input') input: SigninInput) {
    const formatAccount = input.account.toLocaleLowerCase();
    const user: User = await this.userService.findUserByAccount(formatAccount);

    if (!user) {
      throw new NotFoundException('ACCOUNT_FAILED');
    }

    if (user.status === UserStatus.INACTIVE) {
      throw new ForbiddenException('USER_INACTIVE');
    }

    const isPasswordMatching = bcrypt.compareSync(
      input.password,
      user.password,
    );

    const accessToken = this.signAccessToken(user);
    const refreshToken = this.signRefreshToken(user);

    const token = await this.tokenService.findTokenByUserId(user.id);
    if (token) {
      await this.tokenService.updateTokenById(token.id, { refreshToken });
    } else {
      const newToken = {
        userId: user.id,
        refreshToken,
      };
      await this.tokenService.createToken(newToken);
    }

    if (!isPasswordMatching) {
      throw new BadRequestException('PASSWORD_FAILED');
    }

    return { user, accessToken, refreshToken };
  }

  @Mutation(() => SigninDto)
  async refreshToken(@Args('refreshToken') refreshToken: string) {
    const verifyRefreshToken = this.jwtService.verify(refreshToken);
    const user = await this.userService.findUserById(verifyRefreshToken.userId);
    const newRefreshToken = this.signRefreshToken(user);
    const newAccessToken = this.signAccessToken(user);

    await this.tokenService.updateTokenByUserId(user.id, {
      refreshToken: newRefreshToken,
    });
    return { user, accessToken: newAccessToken, refreshToken: newRefreshToken };
  }
}
