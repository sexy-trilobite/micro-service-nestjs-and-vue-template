import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { AppRole } from '../model';
import { Permission } from '../../permission/model'
import { Application } from '../../application/model'
@Injectable()
export class AppRoleService {

    constructor(
        @InjectModel(AppRole.name) private appRoleModel: Model<AppRole>,
        @InjectModel(Permission.name) private permissionModel: Model<Permission>,
        @InjectModel(Application.name) private applicationModel: Model<Application>
    ) { }

    async findAll() {
        return await this.appRoleModel.find()
            .populate([{ path: 'permissions', model: this.permissionModel },
            { path: 'application', model: this.applicationModel }])

            .exec()
    }

}
