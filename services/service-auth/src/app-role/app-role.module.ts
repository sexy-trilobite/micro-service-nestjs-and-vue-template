import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppRoleService } from './service/app-role.service';
import { AppRoleResolver } from './resolver/app-role.resolver';
import { AppRoleSchema, AppRole } from './model';
import { PermissionSchema, Permission } from '../permission/model';
import { ApplicationSchema, Application } from '../application/model';
@Module({
  imports: [
    MongooseModule.forFeature([{ name: AppRole.name, schema: AppRoleSchema }]),
    MongooseModule.forFeature([{ name: Permission.name, schema: PermissionSchema }]),
    MongooseModule.forFeature([{ name: Application.name, schema: ApplicationSchema }]),
  ],
  providers: [AppRoleService, AppRoleResolver]
})
export class AppRoleModule { }
