import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';

@Schema()
export class AppRole extends Document {
    @Prop()
    name: string;

    @Prop()
    description: string;
    @Prop()
    chi_name: string;
    @Prop()
    chi_description: string;

    /* this type of value is Types.ObjectId */
    @Prop({ type: [{ type: Types.ObjectId, ref: 'Permission' }] })
    permissions: string[];

    /* this type of value is string */
    @Prop({ type: String, ref: 'Application' })
    application: string;

}

export const AppRoleSchema = SchemaFactory.createForClass(AppRole);