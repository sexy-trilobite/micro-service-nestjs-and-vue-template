import { Directive, Field, ObjectType, ID } from '@nestjs/graphql';
import { PermissionDto } from '../../../permission/graphql/dto';
import { ApplicationDto } from '../../../application/graphql/dto';

@Directive(`@key(fields: "id")`)
@ObjectType()
export class AppRoleDto {
  @Field(() => ID)
  id: string;

  @Field()
  name: string;

  @Field()
  description: string;

  @Field()
  chi_name: string;

  @Field()
  chi_description: string;

  @Field(() => [String])
  permissions: [string];
  // @Field(() => [PermissionDto])
  // permissions: [PermissionDto];

  @Field(() => ApplicationDto)
  application: ApplicationDto;
}
