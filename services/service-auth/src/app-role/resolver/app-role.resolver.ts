import { Resolver, Query } from '@nestjs/graphql';

import { AppRoleService } from '../service/app-role.service';
import { AppRoleDto } from '../graphql/dto'


@Resolver(() => AppRoleDto)
export class AppRoleResolver {
    constructor(private readonly appRoleService: AppRoleService) { }

    @Query(() => [AppRoleDto])
    async appRoles() {
        return await this.appRoleService.findAll();
    }
}
