import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { PermissionResolver } from './resolver/permission.resolver';
import { PermissionService } from './service/permission.service';
import { Permission, PermissionSchema } from './model';
@Module({
  imports: [
    MongooseModule.forFeature([{ name: Permission.name, schema: PermissionSchema }]),
  ],
  providers: [PermissionResolver, PermissionService]
})
export class PermissionModule { }
