import { Resolver, Query } from '@nestjs/graphql';
import { PermissionService } from '../service/permission.service';
import { PermissionDto } from '../graphql/dto';
@Resolver(() => PermissionDto)
export class PermissionResolver {
    constructor(private readonly permissionService: PermissionService) { }

    @Query(() => [PermissionDto])
    async permissions() {
        return await this.permissionService.findAll();
    }
}
