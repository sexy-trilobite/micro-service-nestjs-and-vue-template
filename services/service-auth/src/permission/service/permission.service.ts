import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Inject, Injectable } from '@nestjs/common';
import { Permission } from '../model';
@Injectable()
export class PermissionService {
    constructor(@InjectModel(Permission.name) private permissionModel: Model<Permission>) { }
    async findAll() {
        return await this.permissionModel.find().exec();
    }
}
