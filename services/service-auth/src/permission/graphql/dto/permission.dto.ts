import { Field, ObjectType, ID } from '@nestjs/graphql';
@ObjectType()
export class PermissionDto {
    @Field(() => ID)
    id: string;

    @Field()
    name: string;

    @Field()
    description: string;

}
