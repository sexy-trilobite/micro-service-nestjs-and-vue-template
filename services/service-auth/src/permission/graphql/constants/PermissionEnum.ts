import { registerEnumType } from '@nestjs/graphql';

export enum PermissionEnum {
    ADMIN_ACCOUNT_MANAGE = '5ac62e238245de2e20435431',
    PERSONAL_ACCOUNT_MANAGE = '5ac62e238245de2e20435432',
    ADMIN_CONTENT_MANAGE = '5ac62e238245de2e20435433',
    PERSONAL_CONTENT_MANAGE = '5ac62e238245de2e20435434'
}
registerEnumType(PermissionEnum, {
    name: 'PermissionEnum',
    description: 'Permission-Id mapping',
});
