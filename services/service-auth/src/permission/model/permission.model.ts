import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, } from 'mongoose';

@Schema()
export class Permission extends Document {
    @Prop()
    name: string;

    @Prop()
    description: string;


}

export const PermissionSchema = SchemaFactory.createForClass(Permission);