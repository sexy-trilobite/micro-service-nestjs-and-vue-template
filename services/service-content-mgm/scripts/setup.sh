#!/usr/bin/env sh
BASEDIR=$(git rev-parse --show-toplevel)
SERVICEDIR=${BASEDIR}/$1
SCRIPTDIR=${SERVICEDIR}/scripts

for d in $(find ${SCRIPTDIR}/* -maxdepth 1 -type d)
do
  SETUPFILE=$d/setup.js
  if [ -f ${SETUPFILE} ]; then
    node ${SETUPFILE}
  fi
done
