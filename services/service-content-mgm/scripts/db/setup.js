const path = require('path');
const dotenv = require('dotenv');
const MongoClient = require('mongodb').MongoClient;
const { Seeder } = require('mongo-seeding');

dotenv.config({ path: path.resolve(__dirname, '../../.env') });

async function main() {
  const {
    DB_HOST,
    DB_PORT,
    DB_NAME,
    DB_ROOT_USER,
    DB_ROOT_PASS,
    DB_ADMIN_USER,
    DB_ADMIN_PASS,
    DB_AUTHSOURCE,
  } = process.env;
  const url = `mongodb://${DB_ROOT_USER}:${DB_ROOT_PASS}@${DB_HOST}:${DB_PORT}/${DB_NAME}?authSource=admin`;

  // Create a new MongoClient
  let client = new MongoClient(url, { useUnifiedTopology: true });

  try {
    console.log(`Setup database ${DB_NAME}`);

    // Connect to the database
    client = await client.connect();

    let db = await client.db('admin').admin();
    const { databases } = await db.listDatabases();

    if (!databases.find((db) => db.name === DB_NAME)) {
      // Create admin user for the service database
      console.log(`Create ${DB_NAME} and admin user`);
      db = await client.db(DB_NAME);
      await db.addUser(DB_ADMIN_USER, DB_ADMIN_PASS, {
        roles: [
          {
            role: 'readWrite',
            db: DB_NAME,
          },
          {
            role: 'dbAdmin',
            db: DB_NAME,
          },
        ],
      });
    }

    console.log('done');
  } catch (err) {
    console.log(err);
  } finally {
    client.close();
  }

  const seeder = new Seeder({
    database: url,
    dropDatabase: true,
  });
  const collections = seeder.readCollectionsFromPath(
    path.resolve('./scripts/db/data'),
    {
      transformers: [Seeder.Transformers.replaceDocumentIdWithUnderscoreId],
    },
  );
  console.log(collections);
  seeder
    .import(collections)
    .then(() => {
      console.log('Success');
    })
    .catch((err) => {
      console.log('Error', err);
    });
}

main();
