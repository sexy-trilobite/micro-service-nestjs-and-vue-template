import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { Post } from '../model'

@Injectable()
export class PostService {
    constructor(
        @InjectModel(Post.name)
        private readonly postModel: Model<Post>,
    ) { }

    async findAll() {
        const posts = await this.postModel.find().exec()
        return posts
    }
}