import {
    Args,
    Mutation,
    Parent,
    Query,
    ResolveField,
    Resolver,
} from '@nestjs/graphql';
import { PostDto } from '../graphql/dto'
import { PostService } from '../service/post.service'

@Resolver()
export class PostResolver {
    constructor(private readonly postService: PostService) {
    }

    @Query(() => [PostDto])
    async posts() {
        return await this.postService.findAll()
    }
}