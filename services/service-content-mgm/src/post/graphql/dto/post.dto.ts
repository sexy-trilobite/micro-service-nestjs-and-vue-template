import { Field, ID, ObjectType } from '@nestjs/graphql';
import { PostStatus } from '../constants/index';

@ObjectType()
export class PostDto {
    @Field(() => ID)
    id: string;

    @Field({ nullable: true })
    title: string;

    @Field({ nullable: true })
    status: PostStatus;
}
