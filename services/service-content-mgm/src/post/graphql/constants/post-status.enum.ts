import { registerEnumType } from '@nestjs/graphql';

export enum PostStatus {
    DONE = 'DONE',
    WAIT = 'WAIT',
}
registerEnumType(PostStatus, {
    name: 'PostStatus',
    description: 'The post status.',
});