import { Document, Types } from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

import { PostStatus } from '../graphql/constants'
@Schema()
export class Post extends Document {
    @Prop()
    title: string;

    @Prop()
    status: PostStatus;
}

export const PostSchema = SchemaFactory.createForClass(Post);
