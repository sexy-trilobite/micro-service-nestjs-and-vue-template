import { NestFactory } from '@nestjs/core';
import { ConfigService } from '@nestjs/config';
import { WINSTON_MODULE_NEST_PROVIDER } from '@payk/nestjs-winston';

import { AppModule } from './app.module';
// import { HttpExceptionFilter } from './common/utils/filter/http-exception.filter';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const configService = app.get(ConfigService);
  app.useLogger(app.get(WINSTON_MODULE_NEST_PROVIDER));
  // app.useGlobalFilters(new HttpExceptionFilter());
  await app.listen(configService.get<string>('APP_PORT'));
  console.log(
    `service-content-mgm run port:${configService.get<string>('APP_PORT')}.`,
  );
}

bootstrap();
