import { join } from 'path';
import * as winston from 'winston';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { GraphQLFederationModule } from '@nestjs/graphql';
import { WinstonModule, winstonConsoleFormat } from '@payk/nestjs-winston';
import { PostModule } from './post/post.module'

@Module({
  imports: [
    WinstonModule.forRootAsync({
      useFactory: async (configService: ConfigService) => {
        const logLevel = configService.get<string>('LOG_LEVEL') || 'debug';
        const logType = configService.get<string>('LOG_TYPE') || 'console';

        let format = null;
        if (logType === 'console') {
          format = winston.format.combine(
            winston.format.timestamp(),
            winston.format.colorize({ all: true }),
            winstonConsoleFormat,
          );
        } else {
          format = winston.format.combine(winston.format.timestamp());
        }
        return {
          level: logLevel,
          defaultMeta: { service: 'content-mgm' },
          transports: [
            new winston.transports.Console({
              format,
            }),
          ],
        };
      },
      inject: [ConfigService],
    }),
    GraphQLFederationModule.forRoot({
      context: ({ req }) => ({ req }),
      autoSchemaFile: join(__dirname, 'schema.gql'),
    }),
    MongooseModule.forRootAsync({
      useFactory: async (configService: ConfigService) => {
        const auth = `${configService.get<string>(
          'DB_ADMIN_USER',
        )}:${configService.get<string>('DB_ADMIN_PASS')}`;
        const host = `${configService.get<string>(
          'DB_HOST',
        )}:${configService.get<string>('DB_PORT')}`;
        const db = configService.get<string>('DB_NAME');
        const uri = `mongodb://${auth}@${host}/${db}`;

        return {
          uri,
          useNewUrlParser: true,
          useUnifiedTopology: true,
          useFindAndModify: false,
        };
      },
      inject: [ConfigService],
    }),
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    PostModule
  ]
})
export class AppModule { }
